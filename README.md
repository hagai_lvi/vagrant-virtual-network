
### What is this repository for? ###

Starting a virtual local network that includes   
 
* 2 client machines
* attacker machine
* snort machine
### How do I start? ###
* install vagrant and virtual box
* clone this repo and `cd` into it
* `vagrant up`.  


The first time you start everything it might take a while, because vagrant will need to download the images and to provision the machines.  
The next will be much faster.  

To stop the VMs `vagrant halt`.  
To re-start them `vagrant up` again.  
To login into the machines `vagrant ssh <machine-name>` where `<machine-name>` is one of `one`, `two`, `attacker` or `snort`.  
To remove all the VMs and all the data, `vagrant destroy` inside the repository dir. **NOTE** that this will destroy **everything** and will remove all the data.
Also note that it will require starting over i.e. - long provisioning time from the beginning.

---
### Notes ###
* Some more advanced topics about vagrant are given inside the `Vagrantfile`.
* The base images are debian jessie as they are lightweight and easily customizable.